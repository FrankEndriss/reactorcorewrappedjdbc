package com.happypeople.reactorcorewrappedjdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.annotation.NonNull;

/**
 * Basic implementation of Connection / PreparedStatement wrapped by Rx-ified
 * api.
 *
 * The idea is to execute all blocking operations in a runtime configurable
 * Scheduler while offering a non-blocking api.
 *
 * Use Mono.subscribeOn(Scheduler) to specify in which Scheduler the blocking
 * calls are executed. This is
 *
 * <pre>
 * - the call to the preparer
 * - the call to the underlying jdbc PreparedStatement.execute()
 * - the iteration over the ResultSet
 * - calls to your rowMapper
 * </pre>
 *
 * Use Observable.observeOn(Scheduler) to specify on which threads your
 * callbacks (i.e. onNext(T)) will be called.*
 *
 * TODO Another implementation which gets Schedulers while creation and uses
 * these to execute the blocking calls, and/or callbacks. This would be useful,
 * because the user would not have to call subscribeOn(sched) on _every_ sql
 * selection, but instead only once in the more configuration oriented part of
 * the application code where the database connection(s) are created.
 */
public class DefaultRxConnection implements RxConnection {
	private final Connection jdbcConnection;

	/**
	 * @param jdbcConnection The delegate.
	 */
	public DefaultRxConnection(@NonNull final Connection jdbcConnection) {
		this.jdbcConnection = jdbcConnection;
	}

	private PreparedStatement doJdbcPrepare(final String sql) {
		try {
			return jdbcConnection.prepareStatement(sql);
		} catch (final SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Mono<RxPreparedStatement> prepareStatement(final String sql) {
		return Mono.defer(() -> Mono.just(new RxPreparedStatementImpl(doJdbcPrepare(sql))));
	}

	private static class RxPreparedStatementImpl implements RxPreparedStatement {
		private final PreparedStatement ps;

		RxPreparedStatementImpl(final PreparedStatement ps) {
			this.ps = ps;
		}

		@Override
		public <T> Flux<T> executeQuery(final StatementPreparer preparer, final RowMapper<T> rowMapper) {
			final Publisher<T> source = new Publisher<T>() {
				@Override
				public void subscribe(final Subscriber<? super T> observer) {
					try {
						if (preparer != null)
							preparer.accept(ps);

						final ResultSet rs = ps.executeQuery();
						while (rs.next())
							observer.onNext(rowMapper.apply(rs));
					} catch (final Throwable e) {
						observer.onError(e);
					}
					observer.onComplete();
				}
			};

			return Flux.defer(() -> source);
		}

		@Override
		public Mono<Long> executeUpdate(final StatementPreparer preparer) {
			return Mono.defer(() -> {
				if (preparer != null)
					preparer.accept(ps);
				try {
					return Mono.just(Long.valueOf(ps.executeUpdate()));
				} catch (final SQLException e) {
					throw new RuntimeException(e);
				}
			});
		}
	}
}
