package com.happypeople.reactorcorewrappedjdbc;

import java.sql.ResultSet;
import java.util.function.Function;

public interface RowMapper<T> extends Function<ResultSet, T> {

}