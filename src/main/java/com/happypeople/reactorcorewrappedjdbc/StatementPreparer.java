package com.happypeople.reactorcorewrappedjdbc;

import java.sql.PreparedStatement;
import java.util.function.Consumer;

public interface StatementPreparer extends Consumer<PreparedStatement> {

}