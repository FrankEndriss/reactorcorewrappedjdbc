package com.happypeople.reactortest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

@DisplayName("Testing Mono and Flux features")
public class TestMonoAndFlux {

	@Test
	@DisplayName("Mono.zipWith(...)")
	void testZipWith() {
		final Mono<Integer> mono1 = Mono.just(Integer.valueOf(1));
		final String result = mono1.zipWith(Mono.just("eins")).map(t -> t.getT1() + " " + t.getT2()).block();
		assertEquals("1 eins", result);
	}

	@Test
	@DisplayName("Mono.zipWhen(...)")
	void testZipWhen() {
		final Mono<Integer> mono1 = Mono.just(Integer.valueOf(1));
		final Tuple2<Integer, String> result = mono1.zipWhen((i) -> Mono.just(strMap(i))).block();
		assertEquals(Integer.valueOf(1), result.getT1());
		assertEquals("eins", result.getT2());
	}

	@Test
	@DisplayName("Mono.zip(...)")
	void testZip() {
		final Mono<Integer> mono1 = Mono.just(Integer.valueOf(1));
		final Tuple2<Integer, String> result = Mono.zip(mono1, Mono.just("eins")).block();
		assertEquals(Integer.valueOf(1), result.getT1());
		assertEquals("eins", result.getT2());
	}

	@Test
	@DisplayName("Flux.mergeOrderedWith(...)")
	void testMergeOrderedWith() {
		final Flux<Integer> flux1 = Flux.just(1, 3);
		final Flux<Integer> flux2 = Flux.just(2, 4);
		final List<Integer> result = flux1.mergeOrderedWith(flux2, Integer::compareTo).collectList().block();
		assertEquals(result.get(0), Integer.valueOf(1));
		assertEquals(result.get(1), Integer.valueOf(2));
		assertEquals(result.get(2), Integer.valueOf(3));
		assertEquals(result.get(3), Integer.valueOf(4));
	}

	private String strMap(final Integer i) {
		switch (i) {
		case 1:
			return "eins";
		case 2:
			return "zwei";
		case 3:
			return "drei";
		case 4:
			return "vier";
		default:
			return "morethanfour";
		}
	}
}
